export default {
  props: {
    formData: {
      type: Object,
      required: true
    }
  },
  computed: {
    $$FormData() {
      return this.formData
    }
  },
  methods: {
    navigateToLink(link) {
      console.log('link: ', link)
      const switchTabs = [
        'pages/tabs/home/home',
        'pages/tabs/category/category',
        'pages/tabs/cart/cart',
        'pages/tabs/mine/mine'
      ]
      const customNavigateTo = (url) => {
        let navigate = switchTabs.findIndex(item => url.indexOf(item) !== -1) !== -1
          ? uni.switchTab
          : uni.navigateTo
        navigate({ url })
      }
      const { type, value } = link
      switch (type) {
        case 'mini-page':
          return uni.navigateTo({ url: `/pages/mini-page/mini-page?id=${value}` })
        case 'shop-home':
          return uni.navigateTo({ url: `/pages/shop/shop?shop_id=${value}` })
        case 'user-center':
          return uni.switchTab({ url: `/pages/tabs/mine/mine` })
        case 'goods':
          return uni.navigateTo({ url: `/goods-module/goods-list` })
        case 'part-goods':
          return uni.navigateTo({ url: `/goods-module/goods?goods_id=${value}` })
        case 'cart':
          return uni.switchTab({ url: `/pages/tabs/cart/cart` })
        case 'category':
          return uni.switchTab({ url: `/pages/tabs/category/category` })
        case 'part-category':
          return uni.navigateTo({ url: `/goods-module/goods-list?category=${value}`})
        case 'coupons':
          return uni.navigateTo({ url: `/promotion-module/coupons/coupons` })
        case 'seckill':
          return uni.navigateTo({ url: `/promotion-module/seckill/seckill` })
        case 'group-buy':
          return uni.navigateTo({ url: `/promotion-module/group-buy/group-buy` })
        case 'custom':
          //#ifdef H5
          return window.open(value)
          //#endif
          return uni.navigateTo({ url: `/pages/mini-page/webview?url=${encodeURIComponent(value)}` })
        case 'mini-program':
          return customNavigateTo(value)
      }
    }
  }
}
