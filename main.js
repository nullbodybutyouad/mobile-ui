import Vue from 'vue'
import App from './App'
import Base64 from './common/base64.min'

Vue.config.productionTip = false
Vue.prototype.Base64 = Base64

App.mpType = 'app'

// 此处为演示Vue.prototype使用，非uView的功能部分
Vue.prototype.vuePrototype = '枣红'

// 引入全局uView
import uView from 'uview-ui'
Vue.use(uView)

// 引入自定义全局uView
// import components from 'components'
// Vue.use(components)

// 此处为演示vuex使用，非uView的功能部分
import store from '@/store'

// 引入uView提供的对vuex的简写法文件
let vuexStore = require('@/store/$u.mixin.js')
Vue.mixin(vuexStore)

// 引入uView对小程序分享的mixin封装
let mpShare = require('uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare)

//进入全局过滤器
import * as filters from '@/utils/filters.js'
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})

//判断是否登录
import { judgeLogin } from '@/utils/login.js';
Vue.prototype.judgeLogin = judgeLogin;
Vue.config.productionTip = false;

//路由
import { navigateTo } from '@/utils/navroter.js';
Vue.prototype.navigateTo = navigateTo;

import MescrollBody from "@/components/mescroll-uni/mescroll-body.vue"
import MescrollUni from "@/components/mescroll-uni/mescroll-uni.vue"
Vue.component('mescroll-body', MescrollBody)
Vue.component('mescroll-uni', MescrollUni)

const app = new Vue({
  store,
  ...App
})

app.$mount()

